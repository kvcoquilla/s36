const express = require('express')
const router = express.Router()
const courseController = require('../controllers/course')
const auth = require('../auth');

router.post('/', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body,userData).then(resultFromController => res.send(resultFromController))
})


router.get('/all',(req,res) => {
	courseController.getAllCourses().then(resultFromController =>res.send(resultFromController))
})

//Retrive all active course
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => 
		res.send(resultFromController));
});

//Retrieve specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params)
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => 
		res.send(resultFromController))
});

//Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

//Archiving a course
router.put('/:courseId/archive', auth.verify,(req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === true) {
		courseController.archiveCourse(req.params,req.body).then(resultFromController =>res.send(resultFromController))
	} 
	else {
		res.send("Not authoized to archive a course")
	}

})

module.exports = router;