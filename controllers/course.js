const Course = require('../models/Course')

module.exports.addCourse = async (reqBody, payload) => {

	if (payload.isAdmin == false) {

		console.log(payload);
		
		return "Not authorized to add course"

	}

	else {
		
		console.log(payload);

		let newCourse =  new Course ({

			name : reqBody.name,
			description: reqBody.description,
			price : reqBody.price

		})

		return newCourse.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return "Course was added"
			}
		})
	}
}

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

//Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

//Retrieve specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		let course = {
			name: result.name,
			price:result.price
		}
		return course

	});
};


//Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	//Syntax:
		//findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}

//Archiving a course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse ={
		isActive :false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course,error) => {
		if(error) {
			return "Cannot archive course"
		} else {
			return "Successfully archived the course."
		}
	})
}
